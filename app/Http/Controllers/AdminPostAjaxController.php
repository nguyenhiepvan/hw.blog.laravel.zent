<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Post;

class AdminPostAjaxController extends Controller
{
    //
	public function index()
	{
    	# code...
		return view('admin_posts_list');
	}

	public function getListPosts()
	{
		# code...
		$posts = Post::orderBy('id','desc')->get();
		//dd($posts);
		return Datatables::of($posts)

		->addIndexColumn()
		->addColumn('action',function ($post)
		{
			# code...
			return
			'<button style="width: 30px;height:30px" data-url="'.route('posts.show',$post->id).'" class="btn btn-show btn-xs btn-success"><i class="fa fa-eye"></i></button>
			<button style="width: 30px;height:30px" data-url="'.route('posts.edit',$post->id).'" class="btn btn-show btn-xs btn-success"><i class="fa fa-pencil"></i></button>
			<button style="width: 30px;height:30px" data-url="'.route('posts.destroy',$post->id).'" class="btn btn-show btn-xs btn-success"><i class="fa fa-trash"></i></button>';
		})
		->editColumn('title',function ($post)
		{
			# code...
			return $post->title;
		})
		->editColumn('description',function ($post)
		{
			# code...
			return Str::words($post->description,25,'...');
		})
		->editColumn('thumbnail',function ($post)
		{
			# code...
			return '<img src="'.$post->thumbnail.'" width="100px" height="100px">';
		})
		->rawColumns(['thumbnail,action'])
		->make(true);
	}
}
