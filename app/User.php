<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;


class user extends Model  implements Authenticatable,CanResetPasswordContract
{
    //
	protected $fillable = ['name', 'email', 'password'];
	use AuthenticableTrait;
	use CanResetPassword;
	use Notifiable;
}
