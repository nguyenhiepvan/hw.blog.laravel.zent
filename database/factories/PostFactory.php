<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
	return [
        //
	'title' => $faker->text($maxNbChars=100),
	'thumbnail' => $faker->imageUrl($width = 708,$height=472),
	'description'=>$faker->text($maxNbChars=150),
	'content'=>$faker->text($maxNbChars=1500),
	'slug'=>$faker->slug(),
	'user_id'=>1,
	'category_id'=>1
	];
});
