@extends('layouts.master_admin')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Quản lý
			<small>Bài viết</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Quản lý bài viết</li>
		</ol>
	</section>
	<table class="table table-hover table-bordered table-striped" id="posts-table">

		<thead>
			<tr>
				<th>STT</th>
				<th>Tiêu đề bài viết</th>
				<th>thumbnail</th>
				<th>description</th>
				<th>#</th>
			</tr>
		</thead>
	</table>
</div>

@endsection