<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('blog.homepage');
// Route::get('/{slug}', 'HomeController@post')->name('blog.post');
// Route::get('/tag/{slug}', 'HomeController@getPostByTag');

Route::prefix('admin')->group(function ()
{
	# code...
	Auth::routes();

	Route::middleware(['auth:web'])->group(function ()
	{
		# code...
		Route::get('/home', 'HomeController@index')->name('home');

		Route::get('posts/getlistposts','AdminPostAjaxController@getListPosts')->name('getlistposts');

		Route::resource('posts','AdminPostAjaxController');
	});
});
